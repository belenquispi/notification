# README #

This README would normally document whatever steps are necessary to get your application up and running.

### PRÁCTICA DE NOTIFICACIONES###

Los pasos más impportantes en esta práctica son:

-   Crear un botón en la view a ser ejecutada, el cual llamará a la actividad de notificación.

-   Importar **UserNotifications** en el ViewController de la vista.

-   Conectar dicho botón con el controlador en modo **Action**.

-   En el método viewDidLoad() se ingresa el código que solicita persmisos al usuario para usar notificaciones, para ello se usa **UNUserNotificationCenter**.

-   Definir una variable de clase de tipo **Bool** e inicializada en false.


-   En la función Action del botón primero verificar que el usuario haya dado autorización para las notificaciones, para ello usar un **if**

-   En la misma función del botón crear una constante de tipo **UNMutableNotificationContent()**, la cual llevará la información de la notificación. 

-   Una notificación consta de:

        -   title
        -   subtitle
        -   body
        -   categoryIdentifier

-   Crear un tirgger, que permita que la notificación se active despues de 10 segundos de que el usuario haya dado clic en el botón. 

-   Finalmente hay que agregar la notificación, con la información antes ingresada.



